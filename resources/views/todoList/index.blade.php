@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>To-do list</h1>
    <h3>{{$article->name_list}}</h3>
    <hr>
@stop

@section('content')
<form action="/todoLists" method="post" enctype="multipart/form-data">
    @csrf
    <div class="mb-3">
        <label for="item_todo" class="form-label">Add new to-do</label>
        <input id="list_id" type="text" name="list_id" value="{{$article->id}}" hidden>
        <input id="item_todo" type="text" name="item_todo" class="form-control" placeholder="Name" tabindex="1">
    </div>
    <button type="submit" id="sub" class="btn btn-primary">Submit</button>
</form>
<table width="100%;" border="0" cellspacing="0" cellpadding="0" class="mt-4">
    <tbody>
        @foreach ($todoLists as $todoList)
            <tr style="cursor:pointer;">
                <td style="width:4%; clear:both; @if($todoList->status == 1) background-color:orange; @endif">
                <p class="check"><label class="switch">
                        <input data-id={{$todoList->id}} class="changeStatus" type="checkbox"
                        data-onstyle="success" data-offstyle="danger" data-toggle="toggle"
                        data-on="Active" data-off="InActive" {{$todoList->status ? 'checked' : ''}} id="changeStatus" value="{{$todoList->id}}">
                        <span class="slider round"></span>
                    </label>
                </p>    
                </td>
                <td style="width:31%;clear:both; @if($todoList->status == 1) background-color:orange; @endif">{{$todoList->name_item}}</td>
                <td id="resp{{$todoList->id}}" style="width:20%; @if($todoList->status == 1) background-color:orange; @endif">
                    <br>
                    @if($todoList->status == 1)
                        <span class="badge badge-danger btnStatus">Completed</span>
                    @else
                        <span class="badge badge-success btnStatus">Item left</span>
                    @endif
                </td>
                <td style="width:10%;clear:both; @if($todoList->status == 1) background-color:orange; @endif">
                    <form action="{{ route('todoLists.destroy', $todoList->id) }}" method="post">
                        @csrf
                        @method('DELETE')
                        <p class="btnDelete">
                            <button type="submit" class="btn btn-sm btn-danger" hidden id="btnDelete{{$todoList->id}}">Delete</button>
                            <button id="btnDel" type="button" onclick="del({{$todoList->id}})" data-toggle="modal" data-target="#modal-delete-med-{{$todoList->id}}" title="Delete to-do list" class="btn btn-sm btn-outline-danger mb-2"><i class="fas fa-times"></i></button>
                        </p>
                    </form>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
@endsection

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <style type="text/css">
        #btnDel {
            background-color: #white;
            border-radius: 6px;
            border: none;
            color: #danger;
            cursor: pointer;
            font-size: 15px;
            padding: 4px 10px;
            text-align: center;
            text-transform: uppercase;
            width: auto;
        }
        .btnDelete{
            position: relative;
            top:13px;
        }
        .button:hover {
            background-color: #FB7E29;
            color: #fff;
        }
        .btnStatus {
        text-align: center;
        position: relative;
        top:-12px;
        }
        .check {
        text-align: center;
        position: relative;
        top:13px;
        }
    </style>
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet"> --}}
@endsection

@section('js')
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="jquery-3.5.1.min.js"></script> --}}

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript">

   

    var page = $(location).attr('href');
    function RefreshTable() {
        setTimeout(5000);
        location.reload();
    }

    $(".changeStatus").click(function() {
        changeStatusList($(this).val());
    });
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    function changeStatusList(item_id){
                    $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': CSRF_TOKEN
                            },
                            url: "{{ url('/todoLists/update/') }}" + '/' + item_id,
                            type: 'post',
                            dataType: 'json',
                            data: {
                            },
                            success: function(result) {
                                console.log(result);
                                    RefreshTable();
                            },
                            error: function(status) {
                                console.log(status);
                            }

                        });
            }

    function del(id){
         const swalWithBootstrapButtons = Swal.mixin({
              customClass: {
                confirmButton: 'btn btn-outline-success m-3',
                cancelButton: 'btn btn-outline-danger m-3'
              },
              buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
              title: '¿Are you sure you want to delete this?',
              // text: "You won't be able to revert this!",
              icon: 'question',
              showCancelButton: true,
              confirmButtonText: 'Yes',
              cancelButtonText: 'Cancel',
              reverseButtons: true,
              // width: '850px'
            })
            .then((result) => {
              if (result.isConfirmed){
                $('#btnDelete'+id).click();
              }
            })
        }
        $("#sub").click(function(e) {
            var item_todo = document.getElementById('item_todo').value;
            if (item_todo.trim() == '') {
                $('#item_todo').addClass('is-invalid').focus();
                Swal.fire({
                    icon: 'warning',
                    title: 'The item of the list is required'
                });
                return false;
            } else {
                $('#item_todo').removeClass('is-invalid');
            }
    });
    </script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
@stop