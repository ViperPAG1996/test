@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Home</h1>
    <h3>To-do list</h3>
    <hr>
@stop

@section('content')
<table class="table table-bordered table mt-4">
    <thead>
        <th scope="col">ID</th>
        <th scope="col">To-do list</th>
        <th scope="col">Action</th>
    </thead>
    <tbody>
        @foreach ($articles as $article)
            <tr>
                <td style="width:25%;clear:both">{{$article->id}}</td>
                <td style="width:25%;clear:both">{{$article->name_list}}</td>
                <td style="width:25%;clear:both">
                    <form action="{{ route('articles.destroy', $article->id) }}" method="post">
                        @csrf
                        @method('DELETE')
                        <a href="/articles/{{$article->id}}/edit"><button type="button" title="Edit to-do list" class="btn btn-outline-warning btn-circle"><i class="fa fa-pencil-alt" aria-hidden="true"></i> </button></a>
                        {{-- <a href="{{ route('todoLists.index',$article->id) }}"><button type="button" title="To-do list" class="btn btn-outline-info btn-circle"><i class="fas fa-eye" aria-hidden="true"></i> </button></a> --}}
                        <a href="/todoLists/?id={{$article->id}}"><button type="button" title="To-do list" class="btn btn-outline-info btn-circle"><i class="fas fa-eye" aria-hidden="true"></i> </button></a>
                        <button type="submit" class="btn btn-danger" hidden id="btnDelete{{$article->id}}">Delete</button>
                        <button type="button" onclick="del({{$article->id}})" data-toggle="modal" data-target="#modal-delete-med-{{$article->id}}" title="Delete to-do list" class="btn btn-outline-danger btn-circle"><i class="fa fa-trash"></i></button>
                    </form>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
@endsection

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script type="text/javascript">
    function del(id){
         const swalWithBootstrapButtons = Swal.mixin({
              customClass: {
                confirmButton: 'btn btn-outline-success m-3',
                cancelButton: 'btn btn-outline-danger m-3'
              },
              buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
              title: '¿Are you sure you want to delete this?',
              // text: "You won't be able to revert this!",
              icon: 'question',
              showCancelButton: true,
              confirmButtonText: 'Yes',
              cancelButtonText: 'Cancel',
              reverseButtons: true,
              // width: '850px'
            })
            .then((result) => {
              if (result.isConfirmed){
                $('#btnDelete'+id).click();
              }
            })
        }
    </script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
@stop