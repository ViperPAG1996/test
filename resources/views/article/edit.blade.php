@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Edit the item selected</h1>
@stop

@section('content')

<form action="/articles/{{$article->id}}" method="post">
    @csrf
    @method('PUT')
    <div class="mb-3">
        <label for="name_list" class="form-label">To-do list name</label>
        <input id="name_list" type="text" name="name_list" class="form-control" placeholder="Name" tabindex="1" value="{{$article->name_list}}">
    </div>
    <a href="/articles" class="btn btn-secondary" tabindex="3"><i class="fa fa-arrow-left"></i> Cancel</a>
    <button type="submit" id="sub" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
</form>
@endsection

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script type="text/javascript">
    $("#sub").click(function(e) {
            var name_list = document.getElementById('name_list').value;
            var description = document.getElementById('description').value;
            if (name_list.trim() == '') {
                $('#name_list').addClass('is-invalid').focus();
                Swal.fire({
                    icon: 'warning',
                    title: 'The name of the list is required'
                });
                return false;
            } else {
                $('#name_list').removeClass('is-invalid');
            }
    });
    </script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    
@stop