<?php

namespace App\Http\Controllers;
use App\Models\TodoList;
use App\Models\Article;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DataTables;

class TodoListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $article = Article::find($request->id);
        $articles = Article::all()->where('user_id',Auth::user()->id);
        
        if($article != null){
            $todoLists = TodoList::all()->where('user_id',Auth::user()->id)->where('list_id', $request->id);
            if($article->user_id == Auth::user()->id){
                return view('todoList.index')->with('article', $article)->with('todoLists',$todoLists);
            } else {
                return view('article.index')->with('articles',$articles);
            }
        } else{
            return view('article.index')->with('articles',$articles);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $todoList = new TodoList();

        $todoList->name_item = $request->item_todo;
        $todoList->list_id = $request->list_id;
        $todoList->user_id = Auth::user()->id;
        $todoList->status = 0;
        $todoList->save();

        $article = Article::find($request->list_id);
        $todoLists = TodoList::all()->where('user_id',Auth::user()->id)->where('list_id', $request->list_id);
        
        return redirect("/todoLists/?id=".$request->list_id)->with('article', $article)->with('todoLists',$todoLists);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $todoList = TodoList::find($id);
        if($todoList->status == 0){
            $todoList->status = 1;
            $todoList->save();
        }else{
            $todoList->status = 0;
            $todoList->save();
        }

        // return response()->json(['var' => ''.$newStatus.'']);
        return Response()->json(["response" => 1], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $todoList = TodoList::find($id);
        $list_id = $todoList->list_id;
        $todoList->delete();
        $article = Article::find($list_id);
        $todoLists = TodoList::all()->where('user_id',Auth::user()->id)->where('list_id', $list_id);
        
        return redirect("/todoLists/?id=".$list_id)->with('article', $article)->with('todoLists',$todoLists);
    }
}
