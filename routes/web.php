<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth')->group(function () {
    Route::get('/', function () {
        return view('auth.login');
    });
    
    Route::resource('articles','App\Http\Controllers\ArticleController');

    Route::resource('todoLists','App\Http\Controllers\TodoListController');

    Route::post('/todoLists/update/{item_id}', 'App\Http\Controllers\TodoListController@update');
    Route::post('/removeStockV/{med_id}/{est_id}', 'establecimientos_has_ventasController@removeStockV');

    // Route::get('/todoLists/{id}', 'App\Http\Controllers\TodolistController@index')->name('todoList.index');

    Route::get('/dash','App\Http\Controllers\ArticleController@index')->name('dash');

    // Route::middleware(['auth:sanctum', 'verified'])->get('/dash', function () {
    //     return view('dash.index');
    // })->name('dash');

});